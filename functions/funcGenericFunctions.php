<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');

function getCurrentPage(){
  return $_SERVER['PHP_SELF'];
}

function getMaxPages($query, $con=NULL){
  if($con == NULL){
    $con = getDatabaseConnection();
  }
  $res = mysqli_query($con,$query);
  return mysqli_num_rows($res)/7;
}

function getPageNumber(){
  if(isset($_GET['page'])){
    return $_GET['page'];
  }else{
    return 1;
  }
}

function getQuery($filter,$table){
  return "SELECT * FROM ".$table." WHERE LIKE %'".mysqli_real_escape_string($filter)."'";
}

function numberOfPages($query){
  $con = getDatabaseConnection();
  $result = mysqli_query($con,$query);
  return mysqli_num_rows($result)/7;
  mysqli_close($con);
}

function getTime(){
  $t=time();
  return date("h:ia , M d, Y",$t);
}

function paginate($pageno, $total_pages){
    print_r('<ul class="pagination"><li class="');
		if($pageno <= 1)
			echo 'disabled';
		print_r('"><a href="?pageno=1">First</a></li>');
    print_r('<li class="');
    if($pageno <= 1){
      echo 'disabled';
    }
    print_r('"><a href="');
    if($pageno <= 1){
      echo '#';
    }else{
      echo '?pageno='.($pageno - 1);
    }
    print_r('">Prev</a></li>');
    print_r('<li class="disabled"><a href="#">Page ' . $pageno . ' of ' . $total_pages . '</a></li>');
    print_r('<li class="');
    if($pageno >= $total_pages){
      echo 'disabled';
    }
    print_r('"><a href="');
    if($pageno >= $total_pages){
      echo '#';
    }else{
      echo '?pageno='.($pageno + 1);
    }
    print_r('">Next</a></li>');
    print_r('<li class="');
    if($pageno >= $total_pages){
      echo 'disabled';
    }
    print_r('"><a href="?pageno='. $total_pages .'">Last</a></li>');
    print_r('</ul>');
}

function paginateResult($pageno, $total_pages, $total_res){
  print_r('<ul class="pagination"><li class="');
  if($pageno <= 1)
    echo 'disabled';
  print_r('"><a href="?results=1">First</a></li>');
  print_r('<li class="');
  if($pageno <= 1){
    echo 'disabled';
  }
  print_r('"><a href="');
  if($pageno <= 1){
    echo '#';
  }else{
    echo '?results='.($pageno - 1);
  }
  print_r('">Prev</a></li>');
  print_r('<li class="disabled"><a href="#">Page ' . $pageno . ' of ' . $total_pages . '</a></li>');
  print_r('<li class="');
  if($pageno >= $total_pages){
    echo 'disabled';
  }
  print_r('"><a href="');
  if($pageno >= $total_pages){
    echo '#';
  }else{
    echo '?results='.($pageno + 1);
  }
  print_r('">Next</a></li>');
  print_r('<li class="');
  if($pageno >= $total_pages){
    echo 'disabled';
  }
  print_r('"><a href="?results='. $total_pages .'">Last</a></li>');
  print_r('</ul>');
  if(isset($_SESSION['searchitem']))
    print_r('<div><p>Search yielded '.$total_res.' result(s) for: <b><i>' . $_SESSION['searchitem'].'</i></b></p></div>');
}

function printTableHead($colArr, $width){
  $html = '';
  $html .= '
    <div class="table-responsive">
    <table class="table table-striped table-condensed table-bordered table-rounded table-sm">
    <thead>
      <tr>';

  for($i = 0; $i < count($colArr); $i++){
    $html .= '<th width="'.$width[$i].'%">'.$colArr[$i].'</th>';
  }

  $html .='</tr>
    </thead>
    <tbody>
    ';
  echo $html;
}

function printTableFoot(){
  echo "</tbody></table></div>";
}

function cleanData($con, $data){
  if(is_array($data)){
    $ret = array();
    foreach($data as $val){
      $s = mysqli_real_escape_string($con, $val);
      if(!$s)
        $ret[] = "NULL";
      else
        $ret[] = $s;
    }
    return $ret;
  }
  else{
    $ret = mysqli_real_escape_string($con, $data);
    if(!$ret){
      $ret = "NULL";
    }
    return $ret;
  }
}

function checkInputByPost($post){
  $count = 0;
  foreach ($post as $input){
    if(!empty($input)){
      $count++;
    }
  }
  return $count;
}
 ?>
