<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');

function accountExists($uname){
  try{
    $con = getDatabaseConnection();
    $query = "SELECT account_id FROM account WHERE username='".mysqli_real_escape_string($con,$uname)."'";
    $result = mysqli_query($con, $query);
    $nrows = mysqli_num_rows($result);
  }catch(Exception $e){
    $nrows = 1;
  }catch(Error $e){
    $nrows = 1;
  }finally{
    mysqli_close($con);
  }
  if($nrows > 0) return 1;
  else return 0;
}

function getAccountIdByAccountName($uname){
  try{
    $con = getDatabaseConnection();
    $query = "SELECT account_id FROM account WHERE account_name='".mysqli_real_escape_string($con,$uname)."'";
    $result = mysqli_query($con, $query);
    $r = mysqli_fetch_array($result);
  }catch(Exception $e){
  }catch(Error $e){
  }finally{
    mysqli_close($con);
  }
  return $r['account_id'];
}

function getPasswordByUsername($uname){
  $con = getDatabaseConnection();
  $query = "SELECT password, account_name FROM account WHERE username='".mysqli_real_escape_string($con, $uname)."'";
  $result = mysqli_query($con,$query);
  // $res = array(0,2,NULL);
  if($result){
    $arr = mysqli_fetch_array($result);
    $res = array("name"=>$arr['account_name'], "password"=>$arr['password']);
  }else{
    $res = array("name"=>NULL, "password"=>NULL);
  }
  mysqli_close($con);
  return $res;
}

function getAccountDetailsByAccountId($uid){
  $con = getDatabaseConnection();
  $query = "SELECT * FROM account WHERE account_id=".mysqli_real_escape_string($con, $uid);
  $result = mysqli_query($con,$query);
  $arr = mysqli_fetch_array($result);
  print_r($query);
  mysqli_close($con);
  return $arr;
}

function getAccountDetailsByAccountName($name){
  $con = getDatabaseConnection();
  $query = "SELECT * FROM account WHERE account_name='".mysqli_real_escape_string($con, $name)."'";
  $result = mysqli_query($con,$query);
  $arr = mysqli_fetch_array($result);
  mysqli_close($con);
  return $arr;
}

function getPrivilegeByAccountName($account){
  $con = getDatabaseConnection();
  $query = "SELECT privilege_id from account WHERE account_name='".mysqli_real_escape_string($con,$account)."'";
  $result = mysqli_query($con,$query);
  $data = mysqli_fetch_array($result);
  mysqli_close($con);
  return $data['privilege_id'];
}

function getAccountSelection(){
	$con = getDatabaseConnection();
	$query = "SELECT account_id, account_name FROM account ORDER BY account_id ASC";
	$result = mysqli_query($con,$query);
	$nrows = mysqli_num_rows($result);
	while ($nrows > 0){
		$row = mysqli_fetch_array($result);
		echo "<option value=".$row['account_id'].">" . $row['account_id']." - ".$row['account_name'] . "</option>";
		$nrows = $nrows - 1;
	}
	mysqli_close($con);
}//end of retrieve account selection function

function getPrivilegeSelection(){
  try{
    $con = getDatabaseConnection();
    $query = "SELECT * FROM privilege ORDER BY privilege_id ASC";
  	$ret = mysqli_query($con, $query);
  	$nr = mysqli_num_rows($ret);
  	while($nr > 0){
  		$rst = mysqli_fetch_array($ret);
  		print_r("<option value=".$rst['privilege_id'].">".$rst['privilege_name']."</option>");
  		$nr -= 1;
  	}
  }catch(Exception $e){
    $_SESSION['query_error'] = $e;
  }catch(Error $e){
    $_SESSION['query_error'] = $e;
  }finally{
    mysqli_close($con);
  }
}

function getAccountPhotoByAccountName($uname){
  try{
    $con = getDatabaseConnection();
    $query = "SELECT photo FROM account WHERE account_name='".mysqli_real_escape_string($con,$uname)."'";
    $result = mysqli_query($con, $query);
    $r = mysqli_fetch_array($result);
  }catch(Exception $e){
  }catch(Error $e){
  }finally{
    mysqli_close($con);
  }
  return $r['photo'];
}

function getAccountsSearchResults(){
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');
  try{
    $con = getDatabaseConnection();
    if(isset($_GET['search'])){
      $search = mysqli_real_escape_string($con, $_GET['search']);
      unset($_GET['search']);

      if (""==trim($search)) {
        unset($_SESSION['searchitem']);
        return 1;
      }
      elseif (!isset($_SESSION['searchitem'])||($search != $_SESSION['searchitem']))
        $_SESSION['searchitem'] = $search;
      else {
        $search = $_SESSION['searchitem'];
      }
    }else{
      if(isset($_SESSION['searchitem']))
        $search = $_SESSION['searchitem'];
      else {
        $search = "";
      }
    }

    $q = "SELECT privilege_id FROM privilege WHERE privilege_name LIKE '%".$search."%'";
    $r = mysqli_query($con, $q);
    if(mysqli_num_rows($r)>0){
      $p = mysqli_fetch_array($r);
      $prv = $p['privilege_id'];
    }else{
      $prv = 0;
    }

    if(isset($_GET['results'])){
      $results = $_GET['results'];
    }else{
      $results = 1;
    }
    $offset = ($results - 1)*10;
    $queryc = "SELECT * FROM account WHERE account_name LIKE '%".$search."%' OR username LIKE '%".$search."%' OR email LIKE '%".$search."%' OR phone LIKE '%".$search."%' OR privilege_id=".$prv;

    $result = mysqli_query($con, $queryc);
    $total_rows = mysqli_num_rows($result);
    $total_pages = ceil($total_rows/10);
    paginateResult($results, $total_pages, $total_rows);

    $colNames = array("Account Number", "Account Name", "Email Address", "Contact Number", "Privilege");
    $width=array(10, 40, 25, 15, 10);
    printTableHead($colNames,$width);

    $query = "SELECT * FROM account WHERE account_name LIKE '%".$search."%' OR username LIKE '%".$search."%' OR email LIKE '%".$search."%' OR phone LIKE '%".$search."%' OR privilege_id=".$prv." ORDER BY account_id ASC LIMIT ".$offset.",10";

    if($r = mysqli_query($con, $query)){
      while($row = mysqli_fetch_array($r)){
        $query = "SELECT privilege_name FROM privilege WHERE privilege_id =" . $row['privilege_id'];
        $r1 = mysqli_query($con, $query);
        $pr = mysqli_fetch_array($r1);
        if ($row['privilege_id'] == 5){
          print_r("<tr class=\"pending\">");
        }else{
          print_r("<tr>");
        }
        print_r("<td>". $row['account_id'] ."</td>");
        print_r("<td>". $row['account_name']."</td>");
        print_r("<td>". $row['email'] . "</td>");
        print_r("<td>". $row['phone'] ."</td>");
        print_r("<td>". $pr['privilege_name']."</th>");
        print_r("</tr>");
      }
    }
    else{
      print_r("<tr>");
      print_r("<th>No query response</th>");
      print_r("</tr>");
    }
  printTableFoot();
  }catch(Exception $e){
    $_SESSION['query_error']="Exception occured: ".$e;
  }catch(Error $e){
    $_SESSION['query_error']="Error occured: ".$e;
  }finally{
    mysqli_close($con);
  }
}
?>
