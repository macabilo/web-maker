<?php
require_once('tcpdf.php');

class MYPDF extends TCPDF {
  protected $hname, $hcn, $hrn, $hem, $hmode;
  protected $ws;
  protected $header_height;

  public function setWs($s){
    $this->ws = $s;
  }

  public function setmode($mode){
    $this->hmode = $mode;
  }

  public function sethname($name){
    $this->hname = $name;
  }

  public function sethcn($cn){
    $this->hcn = $cn;
  }

  public function sethrn($rn){
    $this->hrn = $rn;
  }

  public function sethem($em){
    $this->hem = $em;
  }

  public function getHeaderHeight(){
    return $this->header_height;
  }

  public function Header() {
   if($this->page == 1){
     $lMargin = $this->lMargin;
     $rMargin = $this->rMargin;
     $tMargin = $this->tMargin;
     $bMargin = $this->bMargin;

      $w = $this->getPageWidth()-$lMargin-$rMargin;
      $h = $this->getPageHeight()-$tMargin-$bMargin;

      $currentX = $lMargin;
      $currentY = $tMargin;
      $x = $lMargin;
      $y = $tMargin;
      $maxX = $currentX + $w;
      $maxY = $currentY + $h;

      #logo left
      $imgw = 25;
      $imgh = 25;
      $this->setJPEGQuality(90);
      $logo_filename = $_SERVER['DOCUMENT_ROOT'].'/img/ask.png';
      if (file_exists($logo_filename)) {
          $this->Image($_SERVER['DOCUMENT_ROOT'].'/img/ask.png', $currentX,$currentY, $imgw, $imgh, 'PNG');
      } else {
          $this->SetY(-45);
          $backup_logo = $_SERVER['DOCUMENT_ROOT'].'/img/backup/ask.png';
          if (!copy($backup_logo, $logo_filename)) {
          $this->CreateTextBox('Failed to load image', $currentX,$currentY, $imgw, $imgh, 'PNG');
          } else {
          $this->Image($_SERVER['DOCUMENT_ROOT'].'/img/ask.png', $currentX,$currentY, $imgw, $imgh, 'PNG');
          }
      }
      $currentX+=$imgw;

      #title header
      $textw = $w - 2*$imgw;
      $texth = 4;
      $this->CreateTextBox('THE ROMAN CATHOLIC ARCHDIOCESE OF CEBU, INC.', $currentX, $currentY, $textw, 0, 9, 'B','C');
      $currentY += $texth;
      $this->CreateTextBox('Abtanan sa Kaluoy', $currentX, $currentY, $textw, $texth, 9, '','C');
      $currentY += $texth;
      $this->CreateTextBox('Pope John Paul II Ave., Cor C. Borces St.', $currentX, $currentY, $textw, 0, 9, '','C');
      $currentY += $texth;
      $this->CreateTextBox('Mabolo, 6000 Cebu City', $currentX, $currentY, $textw, 0, 9, '','C');
      $currentY += $texth;
      $this->CreateTextBox('+63 32 254 0951     +63 32 255 4458', $currentX, $currentY, $textw, 0, 9, '','C');
      $currentY += $texth;
      $this->CreateTextBox('abtanansakaluoy@gmail.com', $currentX, $currentY, $textw, 0, 9, '','C');
      $currentX += $textw;
      $currentY = $y;

      #logo right
      $this->setJPEGQuality(90);
      $logo_filename = $_SERVER['DOCUMENT_ROOT'].'/img/archdiocese.png';
      if (file_exists($logo_filename)) {
          $this->Image($_SERVER['DOCUMENT_ROOT'].'/img/archdiocese.png', $currentX,$currentY, $imgw, $imgh, 'PNG');
      } else {
          $backup_logo = $_SERVER['DOCUMENT_ROOT'].'/img/backup/archdiocese.png';
          if (!copy($backup_logo, $logo_filename)) {
          $this->CreateTextBox('Failed to load image', $currentX,$currentY, $imgw, $imgh, 'PNG');
          } else {
          $this->Image($_SERVER['DOCUMENT_ROOT'].'/img/archdiocese.png', $currentX,$currentY, $imgw, $imgh, 'PNG');
          }
      }

      $currentX = $x;
      $currentY += 29;
    }
    else{
      $this->SetMargins(PDF_MARGIN_LEFT,5,PDF_MARGIN_RIGHT);
      $w = $this->getPageWidth();
      $h = $this->getPageHeight();

      $this->CreateTextBox('Child Intake Form: Page '.$this->page.' of '.$this->getAliasNbPages(), 0, 0, $w, 0, 9, '','C');
    }
  }

  public function Footer() {
    $this->writeHTML('<hr>');
    $this->SetXY(0,-10);
    $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 8);
    $this->Cell($this->getPageWidth(), 10, 'Created: '.date('h:ia m-d-Y').'. Prepared by: '.$_SESSION['login-user'], 0, false, 'C');
  }

  public function CreateTextBox($textval, $x = 0, $y, $width = 0, $height = 10, $fontsize = 10, $fontstyle = '', $align = 'L',$cellpadding = 0) {
    $this->SetXY($x, $y);
    $this->SetFont(PDF_FONT_NAME_MAIN, $fontstyle, $fontsize);
    $this->adjustCellPadding(0);
    $this->setCellMargins(0,0,0,0);
    $this->Cell($width, $height, $textval, 0, false, $align);
  }
}
 ?>
