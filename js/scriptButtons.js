$(document).ready(function(){
  $('#register-btn').click(function(){
    location.href="/../pages/pageRegister.php";
  });

  $('#edit-item-element').change(function(){
    $('#edit-submit').prop('disabled', false);
  });

  $('#edit-reset').click(function(){
    $('#edit-form').trigger("reset");
    $('#edit-submit').prop('disabled', true);
    $('#edit-reset').prop('disabled', true);
    $('#edit-item-element').val(0).selectpicker('refresh');
    $('#name-new-element').prop('disabled', true);
    $('#alias-new-element').prop('disabled', true);
    $('#addr1-new-element').prop('disabled', true);
    $('#addr4-new-element').val(0).prop('disabled', true).selectpicker('refresh');
    $('#age-new-element').prop('disabled', true);
    $('.gender-new-element').prop('disabled', true);
    $('input[type=radio][name=religion-new]').prop('disabled', true);
    $('#height-new-element').prop('disabled',true);
    $('#weight-new-element').prop('disabled',true);
    $('#birthdate-new-element').prop('disabled',true);
    $('#baddr1-new-element').prop('disabled', true);
    $('#baddr4-new-element').prop('disabled', true).selectpicker('refresh');
    $('#marks-new-element').prop('disabled',true);
    $('#education-new-element').prop('disabled',true).selectpicker('refresh');
    $('input[type=radio][name=study-new]').prop('disabled', true);
    $('#year-new-element').prop('disabled', true);
    $('#school-new-element').prop('disabled', true);
    $('input[type=radio][name=nourishment-new]').prop('disabled', true);
    $('#add-new-element').prop('disabled', true);
    $('#edit-new-element').prop('disabled', true);
    $('input[type=radio][name=fatherstat-new]').prop('disabled', true);
    $('input[type=radio][name=motherstat-new]').prop('disabled', true);
    $('input[type=radio][name=childstat-new]').prop('disabled', true);
    $('#guardian-new-element').prop('disabled', true);
    $('#relguardian-new-element').prop('disabled', true).selectpicker('refresh');
    $('#gaddr1-new-element').prop('disabled', true);
    $('#gaddr4-new-element').prop('disabled', true).selectpicker('refresh');
    $('input[type=checkbox][name=sleepplace\\[\\]').prop('disabled', true);
    $('input[type=checkbox][name=freqarea\\[\\]]').prop('disabled', true);
    $('input[type=checkbox][name=streetact\\[\\]]').prop('disabled', true);
    $('#reason-new-element').prop('disabled', true);
    $('input[type=radio][name=profilestat-new]').prop('disabled', true);
    $('input[type=checkbox][name=documents\\[\\]]').prop('disabled', true);
    $('input[type=checkbox][name=sacraments\\[\\]]').prop('disabled', true);
    $('#assessment-new-element').prop('disabled', true);
  });

});
