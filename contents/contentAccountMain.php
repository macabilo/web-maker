<div class="row" style="margin-top:15px;padding-left:5%; padding-right:5%;">
  <?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');
  if(!isset($_GET['search']) && !isset($_GET['results'])){
    include($_SERVER['DOCUMENT_ROOT'].'/tables/tableAccountList.php');
  }else{
    $nosearch = getAccountsSearchResults();
    if($nosearch == 1){
      unset($_GET['search']);
      unset($_GET['results']);
      include($_SERVER['DOCUMENT_ROOT'].'/tables/tableAccountList.php');
    }
  }
  ?>
  <?php  ?>
</div>
<div class="row" style="margin-top:20px;">
</div>
