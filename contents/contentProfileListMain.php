<div class="row" style="margin-top:15px;padding-left:5%; padding-right:5%;">
  <div id="main-table">
  <?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcProfileListMngt.php');
  if(!isset($_GET['search']) && !isset($_GET['results'])){
    include($_SERVER['DOCUMENT_ROOT'].'/tables/tableProfileList.php');
  }else{
    $nosearch = getProfileListSearchResults();
    if($nosearch == 1){
      unset($_GET['search']);
      unset($_GET['results']);
      include($_SERVER['DOCUMENT_ROOT'].'/tables/tableProfileList.php');
    }
  }
  ?>
  </div>
  <div id="details"></div>
  <div id="btn-details"></div>
</div>

<div class="row" style="margin-top:20px;">
</div>
