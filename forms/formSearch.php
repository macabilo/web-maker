<?php $page = htmlentities($_SERVER['PHP_SELF']); ?>
<form action="<?php echo $page;?>" method="get">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Type search keyword" name="search">
    <div class="input-group-btn">
      <button class="btn btn-default" type="submit">
        <i class="glyphicon glyphicon-search"></i>
      </button>
    </div>
  </div>
</form>
