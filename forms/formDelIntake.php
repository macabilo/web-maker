<form action="/../procs/procProfileListMngt?delitem" method="post">

  <!-- item to remove -->
  <div class="form-group">
    <label class="control-label" for="del-item-element">Select profile</label>
    <select class="form-control selectpicker" id="del-item-element" name="profile_id" data-live-search="true" data-size="7" required>
      <?php
      require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcProfileListMngt.php');
      getActiveProfileListSelection(); ?>
    </select>
  </div>

  <!-- buttons -->
  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
