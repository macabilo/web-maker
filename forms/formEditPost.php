<form action="/../procs/procPostsMngt.php?editpost=<?php echo $post['post_id'] ?>" method="post">
  <!-- Title -->
  <div class="form-group">
    <label class="control-label" for="title-element">Title</label>
    <input type="text" class="form-control" id="title-element" name="title" value="<?php echo $post['title'] ?>">
  </div>

  <!-- Body -->
  <div class="form-group">
    <label class="control-label" for="body-element">Body</label>
    <textarea class="form-control" row="3" id="body-element" name="body" style="resize:none;" required><?php echo $post['body'] ?></textarea>
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
