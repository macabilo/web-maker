<form action="/../procs/procAccountsMngt?editacc" method="post">

  <!-- item select -->
  <div class="form-group">
    <label class="control-label" for="select-account">Select Existing Account</label>
    <select class="form-control selectpicker" id="select-account-element" name="aid" data-live-search="true" data-size="7" required>
      <option selected disabled>-- select account to edit --</option>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php'); getAccountSelection(); ?>
    </select>
  </div>

  <!-- Privilege -->
  <div class="form-group">
    <label class="control-label" for="new-priv">Privilege</label>
    <select class="form-control selectpicker" id="new-priv" name="priv" data-live-search="true" data-size="5" required>
      <option selected disabled>-- select account first --</option>
    </select>
  </div>

  <!-- buttons -->
  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
