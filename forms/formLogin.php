<form action="/../procs/procLogin.php?login" method="post">
  <div class="form-group row">
    <label for="username" class="col-sm-4 col-form-label">Username: </label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="username" name="username" placeholder="username" maxlength="30" value="<?php if(isset($_POST['username'])) echo $_POST['username']; ?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="password" class="col-sm-4 col-form-label">Password: </label>
    <div class="col-sm-8">
      <input type="password" class="form-control" id="password" name="password" placeholder="password" maxlength="20" value="<?php if(isset($_POST['password'])) echo $_POST['password']; ?>">
    </div>
  </div>
  </br>

  <div class="form-row">
    <div class="col-md-2"></div>
    <button type="submit" name="login" class="btn btn-default col-md-2 col-sm-4 col-xs-12 row-button">Log in</button>
    <div class="col-md-1"></div>
    <button type="button" class="btn btn-default col-md-2 col-sm-4 col-xs-12 row-button" id="register-btn">Sign up</button>
    <div class="col-md-1"></div>
</form>
      <button type="button" class="btn btn-default col-md-3 col-sm-4 col-xs-12 row-button" data-toggle="modal" data-target="#modal-resetpass-element">Password Reset</button>
      <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalResetPass.php'); ?>
    <div class="col-md-1"></div>
  </div>
</div>
