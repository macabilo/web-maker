<form action="/../procs/procPostsMngt.php?addpost" method="post">
  <!-- Title -->
  <div class="form-group">
    <label class="control-label" for="title-element">Title</label>
    <input type="text" class="form-control" id="title-element" name="title" placeholder="Give your post a title" required>
  </div>

  <!-- Body -->
  <div class="form-group">
    <label class="control-label" for="body-element">Body</label>
    <textarea class="form-control" row="3" id="body-element" name="body" placeholder="Type your post (max 255 chars)" style="resize:none;" required></textarea>
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
