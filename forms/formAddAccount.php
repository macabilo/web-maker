<form action="/../procs/procAccountsMngt?addacc" method="post">
  <!-- Full name -->
  <div class="form-group">
    <label class="control-label" for="fname-element">Full Name</label>
    <input type="text" class="form-control" id="fname-element" name="fname" placeholder="Enter new user's full name" required>
  </div>

  <!-- Username -->
  <div class="form-group">
    <label class="control-label" for="uname-element">Username</label>
    <input type="text" class="form-control" id="uname-element" name="uname" placeholder="Enter new user's unique username" required>
  </div>

  <!-- Password -->
  <div class="form-group">
    <label class="control-label" for="pword-element">Password</label>
    <input type="password" class="form-control" id="pword-element" name="pword" placeholder="Enter new user's password" required>
  </div>

  <!-- Contact Number -->
  <div class="form-group">
    <label class="control-label" for="cnum-element">Contact Number</label>
    <input type="number" class="form-control" name="cnum" id="cnum-element" placeholder="Enter new user's contact number"> <!--not required for no cp-->
  </div>

  <!-- Email -->
  <div class="form-group">
    <label class="control-label" for="email-element">Email Address</label>
    <input type="email" class="form-control" id="email-element" name="email" placeholder="Enter new user's email address">
  </div>

  <!-- Privilege -->
  <div class="form-group">
    <label class="control-label" for="privilege-element">Privilege</label>
    <select class="form-control selectpicker" id="privilege-element" name="priv" data-live-search="true" data-size="5" required>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');
      getPrivilegeSelection();
      ?>
    </select>
  </div>

  <!-- buttons -->
  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
