<?php include($_SERVER['DOCUMENT_ROOT'].'/procs/procSessionCheck.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/headContent.php'); ?>
</head>

<body>
  <div class="container-fluid">
    <!-- header -->
    <div class="row">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <!-- Insert header style -->
          <?php include($_SERVER['DOCUMENT_ROOT'].'/headers/headerLogin.php'); ?>
        </div>
      </nav>
    </div>

    <!-- main body -->
    <div class="row" style="padding-top:50px;">
      <!-- Left-side column section-->
      <div class="col-sm-2 col-md-3"></div>

      <!-- Center column section-->
      <div class="col-sm-8 col-md-6">
        <h2 class="h2 text-center">Log In to Your Account</h2></br>
        <?php include($_SERVER['DOCUMENT_ROOT'].'/forms/formLogin.php'); ?>
      </div>

      <!-- Right-side column section-->
      <div class="col-sm-2 col-md-3"></div>
    </div>

    <!-- Footer section-->
    <footer class="text-center row" style="padding-bottom:1px; padding-top:8px;">
      <?php
      include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
      include($_SERVER['DOCUMENT_ROOT'].'/notifications.php');
      ?>
    </footer>
  </div>
</body>
