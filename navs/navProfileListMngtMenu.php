<!-- search bar -->
<div class="row" style="padding-top:50px;padding-left:5%;padding-right:5%;">
  <?php include($_SERVER['DOCUMENT_ROOT'].'/forms/formSearch.php'); ?>
</div>

<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');
if (getPrivilegeByAccountName($_SESSION['login-user']) < 2){ ?>
<!-- inventory options -->

<div class="row" style="padding-top:5%;padding-left:5%;padding-right:5%;">

  <div class="menu-group">
    <!-- group heading -->
    <h5 class="text-center">List Management</h5>

    <!-- add item button -->
    <button type="button" class="btn btn-default btn-sm col-xs-6" data-toggle="modal" data-target="#new-intake-modal">Add New Profile</button>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalNewIntake.php'); ?>

    <!-- edit item button -->
    <button type="button" class="btn btn-default btn-sm col-xs-6" data-toggle="modal" data-target="#edititem">Edit Profile</button>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalEditIntake.php'); ?>

    <!-- delete item button -->
    <button type="button" class="btn btn-default btn-sm col-xs-6" data-toggle="modal" data-target="#rstitem">Reactivate Profile</button>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalRstIntake.php'); ?>

    <!-- delete item button -->
    <button type="button" class="btn btn-default btn-sm col-xs-6" data-toggle="modal" data-target="#delitem">Deactivate Profile</button>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalDelIntake.php'); ?>

    <!-- print list button -->
    <button type="button" class="btn btn-default btn-sm col-xs-6" data-toggle="modal" data-target="#printlist">Print Profile</button>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalPrintProfile.php'); ?>

  </div>
</div>
<?php }?>
<?php if (getPrivilegeByAccountName($_SESSION['login-user']) < 4){?>
<div class="row" style="padding-left:5%;padding-right:5%;">
  <!-- modify display switch -->
  <div class="menu-group">
    <label class="checkbox-inline">
      <input type="checkbox" data-toggle="toggle" id="active-view" data-size="small" data-off="All profiles are currently shown" data-on="Only active profiles are now shown" data-onstyle="primary" data-offstyle="info" data-width="650%">
    </label>
  </div>
</div>
<?php }?>
