<?php require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');
if(getPrivilegeByAccountName($_SESSION['login-user']) < 3){ ?>
<!-- accounts options -->
<div class="row" style="padding-top:5%;padding-left:5%;padding-right:5%;">
  <div class="menu-group">
    <!-- group heading -->
    <h5 class="text-center">Personal Profile Management</h5>

    <!-- add account button -->
    <button type="button" class="btn btn-default btn-sm col-xs-12" data-toggle="modal" data-target="#modal-edit-personal-element">Edit Personal Info</button>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalEditPersonal.php'); ?>

  </div>
</div>
<?php } ?>
