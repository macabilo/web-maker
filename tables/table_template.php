<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/database_conn.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/func_generic.php');

try{
  # setup pagination
  $con = getDatabaseConnection();
  if(isset($_GET['pageno'])){
    $pageno = $_GET['pageno'];
  }else{
    $pageno = 1;
  }
  $offset = ($pageno - 1)*10;
  $query = "SELECT COUNT(*) FROM inventory"; //replace database table name
  $result = mysqli_query($con, $query);
  $total_rows = mysqli_fetch_array($result);
  $total_pages = ceil($total_rows[0]/10);
  paginate($pageno, $total_pages);

  # print table head
  $colNames = array("Profile No.","First Name", "Last Name"); //replace desired column names
  $widths=array(20, 40, 40); //change to desired widths
  printTableHead($colNames, $widths);

  # generate table content
  // $query = "SELECT i.* FROM inventory AS i JOIN materials AS ma ON ma.materialID=i.materialID JOIN models AS mo ON mo.modelID=i.modelID ORDER BY ma.materialName ASC, mo.modelName ASC, i.serialNumber ASC LIMIT " . $offset . ", 10";
  $query = "SELECT * from inventory LIMIT " . $offset .", 10";
  $ret = mysqli_query($con, $query);
  $nrows = mysqli_num_rows($ret);
  while($nrows > 0){
    $row = mysqli_fetch_array($ret);
    // $query = "SELECT materialName FROM materials WHERE materialID =" . $row['materialID'];
    // $r1 = mysqli_query($con, $query);
    // $mat = mysqli_fetch_array($r1);
    // $query = "SELECT modelName FROM models WHERE modelID =" . $row['modelID'];
    // $r2 = mysqli_query($con, $query);
    // $mod = mysqli_fetch_array($r2);
    // $query = "SELECT realname FROM accounts WHERE accountID =" . $row['custodian'];
    // $r3 = mysqli_query($con, $query);
    // $cus = mysqli_fetch_array($r3);
    print_r("<tr>");
    print_r("<th>". $row['logId'] ."</th>");
    print_r("<th>". $row['firstName']."</th>");
    print_r("<th>". $row['lastName'] . "</th>");
    // print_r("<th>". $row['dateAcquired'] ."</th>");
    // print_r("<th>". $cus['realname']."</th>");
    // print_r("<th>". $row['remarks'] . "</th>");
    print_r("</tr>");
    $nrows -= 1;
  }
  printTableFoot();
}catch(Exception $e){

}catch(Error $e){

}finally{
  mysqli_close($con);
}

?>
