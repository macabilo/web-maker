<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');

try{
  # setup pagination
  $con = getDatabaseConnection();

  $q = "SELECT show_active FROM settings ORDER BY setting_id DESC LIMIT 1";
  $r = mysqli_query($con, $q);
  $d = mysqli_fetch_array($r);

  if($d['show_active'] == 0){
    if(isset($_GET['pageno'])){
      $pageno = $_GET['pageno'];
    }else{
      $pageno = 1;
    }
    $offset = ($pageno - 1)*10;
    $query = "SELECT COUNT(*) FROM profile"; //replace database table name
    $result = mysqli_query($con, $query);
    $total_rows = mysqli_fetch_array($result);
    $total_pages = ceil($total_rows[0]/10);
    paginate($pageno, $total_pages);

    # print table head
    $colNames = array("Profile No.", "Name","Status"); //replace desired column names
    $widths=array(20, 60, 20); //change to desired widths
    printTableHead($colNames, $widths);

    # generate table content
    $query = "SELECT * from profile LIMIT " . $offset .", 10";
    $ret = mysqli_query($con, $query);
    $nrows = mysqli_num_rows($ret);
    while($nrows > 0){
      $row = mysqli_fetch_array($ret);
      if($row['status_id'] == 'New' || $row['status_id'] == 'Returnee' || $row['status_id'] == 'Old'){
        print_r("<tr class=\"clickable success\">");
        print_r("<td>". $row['profile_id'] ."</td>");
        print_r("<td>". $row['name']."</td>");
        print_r("<td>Active</td>");
      }
      else {
        print_r("<tr class=\"clickable danger\">");
        print_r("<td>". $row['profile_id'] ."</td>");
        print_r("<td>". $row['name']."</td>");
        print_r("<td>Inactive</td>");
      }
      print_r("</tr>");
      $nrows -= 1;
    }
    printTableFoot();
  }else{
    if(isset($_GET['pageno'])){
      $pageno = $_GET['pageno'];
    }else{
      $pageno = 1;
    }
    $offset = ($pageno - 1)*10;
    $query = "SELECT COUNT(*) FROM active_profiles"; //replace database table name
    $result = mysqli_query($con, $query);
    $total_rows = mysqli_fetch_array($result);
    $total_pages = ceil($total_rows[0]/10);
    paginate($pageno, $total_pages);

    # print table head
    $colNames = array("Profile No.", "Name","Status"); //replace desired column names
    $widths=array(20, 60, 20); //change to desired widths
    printTableHead($colNames, $widths);

    # generate table content
    $query = "SELECT profile_id from active_profiles LIMIT " . $offset .", 10";
    $ret = mysqli_query($con, $query);
    $nrows = mysqli_num_rows($ret);
    while($nrows > 0){
      $row = mysqli_fetch_array($ret);
      $nq = "SELECT * FROM profile WHERE profile_id =".$row['profile_id'];
      $nr = mysqli_query($con, $nq);
      $nrow = mysqli_fetch_array($nr);
      print_r("<tr class=\"clickable success\">");
      print_r("<td>". $row['profile_id'] ."</td>");
      print_r("<td>". $nrow['name']."</td>");
      print_r("<td>Active</td>");
      print_r("</tr>");
      $nrows -= 1;
    }
    printTableFoot();
  }

}catch(Exception $e){

}catch(Error $e){

}finally{
  mysqli_close($con);
}

?>
