<?php
include($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');
include($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');
include($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');

if(isset($_POST['selected']) && !empty($_POST['selected'])){
	unset($_POST['selected']);

	try{
		$con = getDatabaseConnection();

    if(isset($_POST['queryid'])){
		  $query = "SELECT * FROM profile WHERE profile_id=".cleanData($con, $_POST['queryid']);
    }else if(isset($_POST['queryname'])){
      $query = "SELECT * FROM profile WHERE name='".cleanData($con, $_POST['queryname'])."'";
    }

		$result = mysqli_query($con, $query);
		$data = mysqli_fetch_array($result);

		$q = "SELECT DATE_FORMAT('".$data['birthdate']."', '%d %M %Y') AS bd FROM profile WHERE profile_id=".$data['profile_id'];
		$r = mysqli_query($con, $q);
		$bd = mysqli_fetch_array($r, MYSQLI_ASSOC);
		$bd = $bd['bd'];
		$today = new DateTime(date('d.m.y'));
		$age = date_diff(new DateTime($bd), $today, true);
		$age = $age->format('%y');

		$q = "SELECT * FROM address WHERE address_id =". $data['address_id'];
		$r = mysqli_query($con, $q);
		$addr = mysqli_fetch_array($r);
		$q = "SELECT * FROM barangay WHERE barangay_id =".$addr['barangay_id'];
		$r = mysqli_query($con, $q);
		$bgy = mysqli_fetch_array($r);
		$q = "SELECT * FROM locality WHERE locality_id =".$bgy['locality_id'];
		$r = mysqli_query($con, $q);
		$loc = mysqli_fetch_array($r);
		$q = "SELECT * FROM province WHERE province_id =".$loc['province_id'];
		$r = mysqli_query($con, $q);
		$pro = mysqli_fetch_array($r);
		$addr_str = ucwords($addr['street_name']).", ".ucwords($bgy['barangay_name']).", ".ucwords($loc['locality_name']).", ".ucwords($pro['province_name']);

		$q = "SELECT * FROM address WHERE address_id =". $data['birthplace'];
		$r = mysqli_query($con, $q);
		$addr = mysqli_fetch_array($r);
		$q = "SELECT * FROM barangay WHERE barangay_id =".$addr['barangay_id'];
		$r = mysqli_query($con, $q);
		$bgy = mysqli_fetch_array($r);
		$q = "SELECT * FROM locality WHERE locality_id =".$bgy['locality_id'];
		$r = mysqli_query($con, $q);
		$loc = mysqli_fetch_array($r);
		$q = "SELECT * FROM province WHERE province_id =".$loc['province_id'];
		// print_r("q3:".$q);
		$r = mysqli_query($con, $q);
		$pro = mysqli_fetch_array($r);
		$baddr_str = ucwords($addr['street_name']).", ".ucwords($bgy['barangay_name']).", ".ucwords($loc['locality_name']).", ".ucwords($pro['province_name']);

		$q = "SELECT * FROM profile_relatives WHERE profile_id =".$data['profile_id'];
		$rels = mysqli_query($con, $q);
		$n = mysqli_num_rows($rels);
		$rel_str = "";
		while($n > 0){
			$prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
			$q = "SELECT * FROM relative WHERE relative_id=".$prof['relative_id'];
			$r = mysqli_query($con, $q);
			$rel = mysqli_fetch_array($r);
			$q = "SELECT * FROM relationship WHERE relationship_id=".$rel['relationship_id'];
			$r = mysqli_query($con, $q);
			$rv = mysqli_fetch_array($r);
			$q = "SELECT * FROM address WHERE address_id=".$rel['baddr_id'];
			$r = mysqli_query($con,$q);
			$ba = mysqli_fetch_array($r);
			$q = "SELECT * FROM locality WHERE locality_id =".$ba['locality_id'];
			$r = mysqli_query($con, $q);
			$loc = mysqli_fetch_array($r);
			$q = "SELECT * FROM province WHERE province_id =".$loc['province_id'];
			$r = mysqli_query($con, $q);
			$pro = mysqli_fetch_array($r);
			$rel_str .= ucwords($rel['relative_name'])." (".$rv['relationship_name'].") - ".$rel['contact']." - ".ucwords($loc['locality_name']).", ".ucwords($pro['province_name'])."</br>";
			$n--;
		}

		$q = "SELECT * FROM profile_sleep_areas WHERE profile_id =".$data['profile_id'];
		$rels = mysqli_query($con, $q);
		$n = mysqli_num_rows($rels);
		$slp_str = "";
		while($n > 0){
			$prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
			$q = "SELECT * FROM sleeping_area WHERE area_id=".$prof['area_id'];
			$r = mysqli_query($con, $q);
			$slp = mysqli_fetch_array($r);
			$slp_str .= ucwords($slp['area_name'])."</br>";
			$n--;
		}

		$q = "SELECT * FROM profile_work_areas WHERE profile_id =".$data['profile_id'];
		$rels = mysqli_query($con, $q);
		$n = mysqli_num_rows($rels);
		$wrk_str = "";
		while($n > 0){
			$prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
			$q = "SELECT * FROM working_area WHERE area_id=".$prof['area_id'];
			$r = mysqli_query($con, $q);
			$slp = mysqli_fetch_array($r);
			$wrk_str .= ucwords($slp['area_name'])."</br>";
			$n--;
		}

		$q = "SELECT * FROM profile_activities WHERE profile_id =".$data['profile_id'];
		$rels = mysqli_query($con, $q);
		$n = mysqli_num_rows($rels);
		$act_str = "";
		while($n > 0){
			$prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
			$q = "SELECT * FROM activity WHERE activity_id=".$prof['activity_id'];
			$r = mysqli_query($con, $q);
			$slp = mysqli_fetch_array($r);
			$act_str .= ucwords($slp['activity_name'])."</br>";
			$n--;
		}

		$q = "SELECT * FROM profile_documents WHERE profile_id =".$data['profile_id'];
		$rels = mysqli_query($con, $q);
		$n = mysqli_num_rows($rels);
		$doc_str = "";
		while($n > 0){
			$prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
			$q = "SELECT * FROM document WHERE document_id=".$prof['document_id'];
			$r = mysqli_query($con, $q);
			$slp = mysqli_fetch_array($r);
			$doc_str .= ucwords($slp['document_name'])."</br>";
			$n--;
		}

		$q = "SELECT * FROM profile_sacraments WHERE profile_id =".$data['profile_id'];
		$rels = mysqli_query($con, $q);
		$n = mysqli_num_rows($rels);
		$sac_str = "";
		while($n > 0){
			$prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
			$q = "SELECT * FROM sacrament WHERE sacrament_id=".$prof['sacrament_id'];
			$r = mysqli_query($con, $q);
			$slp = mysqli_fetch_array($r);
			$sac_str .= ucwords($slp['sacrament_name'])."</br>";
			$n--;
		}

		$q = "SELECT * FROM relative WHERE relative_id =".$data['guardian_id'];
		$r = mysqli_query($con, $q);
		$gdn = mysqli_fetch_array($r);
		$q = "SELECT * FROM relationship WHERE relationship_id=".$gdn['relationship_id'];
		$r = mysqli_query($con, $q);
		$rv = mysqli_fetch_array($r);
		$q = "SELECT * FROM address WHERE address_id=".$gdn['baddr_id'];
		$r = mysqli_query($con,$q);
		$ba = mysqli_fetch_array($r);
		$q = "SELECT * FROM locality WHERE locality_id =".$ba['locality_id'];
		$r = mysqli_query($con, $q);
		$loc = mysqli_fetch_array($r);
		$q = "SELECT * FROM province WHERE province_id =".$loc['province_id'];
		// print_r("q3:".$q);
		$r = mysqli_query($con, $q);
		$pro = mysqli_fetch_array($r);
		$gdn_str = ucwords($gdn['relative_name'])." (".$rv['relationship_name'].") - ".$rel['contact']." - ".ucwords($loc['locality_name']).", ".ucwords($pro['province_name'])."</br>";

		$q = "SELECT * FROM education WHERE education_id =".$data['education_id'];
		$r = mysqli_query($con, $q);
		$gdn = mysqli_fetch_array($r);
		$q = "SELECT * FROM educational_level WHERE level_id=".$gdn['level_id'];
		$r = mysqli_query($con, $q);
		$relg = mysqli_fetch_array($r);
		if($gdn['last_year_attended'] == '1970'){
			$edu_str = $relg['level_name'];
		}else if($gdn['still_studying'] == 'Yes'){
			$edu_str	= $relg['level_name']." (currently studying)";
		}else{
			$edu_str = $relg['level_name']." (".$gdn['last_year_attended'].", ".ucwords($gdn['last_school_attended']).")";
		}

		$q = "SELECT * FROM account WHERE account_id =".$data['volunteer_id'];
		$r = mysqli_query($con, $q);
		$vol = mysqli_fetch_array($r);

	  $imgLocation = $_SERVER['SERVER_NAME'].$data['profile_picture']."?".time();;
		$bimgLocation = $_SERVER['SERVER_NAME'].$data['fbody_picture']."?".time();

    $html = "";
    $html .= '
			<div class="row" style="padding-top:5%;padding-left:0%;padding-right:0%;">
				<button type="button" class="btn btn-default btn-sm remove-details">Back</button>
				<button type="button" class="btn btn-default btn-sm profile-upload" data-toggle="modal" data-target="#modal-photo-upload-element">Upload profile photo</button>
				<div id="modal-photo-upload-element" class="modal fade" role="dialog">
				  <div class="modal-dialog modal-md">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Upload Profile Photo</h4>
				      </div>
				      <div class="modal-body">
							<form action="/../procs/procProfileListMngt.php?uploadPhoto" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label class="control-label" for="photo-element">Select Photo (max 50MB)</label>
									<input id="photo-element" name="photo" type="file" data-show-preview="true">
									<input name="profile" value="'.$data['profile_id'].'" hidden>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary btn-sm">Submit</button>
									<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
								</div>
							</form>
				      </div>
				    </div>
				  </div>
				</div>
				<button type="button" class="btn btn-default btn-sm profile-upload" data-toggle="modal" data-target="#modal-bphoto-upload-element">Upload full body photo</button>
				<div id="modal-bphoto-upload-element" class="modal fade" role="dialog">
				  <div class="modal-dialog modal-md">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Upload Full Body Photo</h4>
				      </div>
				      <div class="modal-body">
							<form action="/../procs/procProfileListMngt.php?uploadBPhoto" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label class="control-label" for="photo-element-fb">Select Photo (max 50MB)</label>
									<input id="photo-element-fb" name="photo" type="file" data-show-preview="true">
									<input name="profile" value="'.$data['profile_id'].'" hidden>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary btn-sm">Submit</button>
									<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
								</div>
							</form>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
			<div class="row" style="padding-top:2%;padding-left:0%;padding-right:0%;padding-bottom:2%;width: 100%;height:400px;">
				<img class="detail-photo" src="http://'. $imgLocation.'" alt="'.$imgLocation.'.jpg" id="profile-picture-element">
				<img class="detail-photo" src="http://'. $bimgLocation.'" alt="'.$bimgLocation.'.jpg" id="fbody-picture-element">

				<div class="modal fade" id="pp-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title" id="myModalLabel">Image preview: Profile Picture</h4>
				      </div>
				      <div class="modal-body">
				        <img src="" id="pp-preview" style="width: 100%; height: 100%;" >
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>
				  </div>
				</div>

				<div class="modal fade" id="fb-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title" id="myModalLabel">Image preview: Full-body Picture</h4>
				      </div>
				      <div class="modal-body">
				        <img src="" id="fb-preview" style="width: 100%; height: 100%;" >
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>
				  </div>
				</div>


			</div>
			<div class="row">
				<div class="col-md-1 col-lg-2"></div>
				<div class="col-md-10 col-lg-8">
		      <table class="table table-striped table-condensed table-bordered table-rounded table-sm">
		      <thead>
		        <tr>
		          <th style="width: 50%">Info</th>
		          <th style="width: 50%">Details</th>
		        </tr>
		      </thead>
		      <tbody>
					<tr>
						<td>Profile ID</td>
						<td>'.$data['profile_id'].'</td>
					</tr>
					<tr>
						<td>Child\'s Name</td>
						<td>'.ucwords($data['name']).'</td>
					</tr>
					<tr>
						<td>Alias</td>
						<td>'.ucwords($data['alias']).'</td>
					</tr>
					<tr>
						<td>Age</td>
						<td>'.$age.'</td>
					</tr>
					<tr>
						<td>Contact Number</td>
						<td>'.$data['child_phone'].'</td>
					</tr>
					<tr>
						<td>Address</td>
						<td>'.$addr_str.'</td>
					</tr>
					<tr>
						<td>Birthdate</td>
						<td>'.$bd.'</td>
					</tr>
					<tr>
						<td>Birthplace</td>
						<td>'.$baddr_str.'</td>
					</tr>
					<tr>
						<td>Gender</td>
						<td>'.ucwords($data['gender']).'</td>
					</tr>
					<tr>
						<td>Religion</td>
						<td>'.ucwords($data['religion']).'</td>
					</tr>
					<tr>
						<td>Height (cm)</td>
						<td>'.$data['height'].'</td>
					</tr>
					<tr>
						<td>Weight (kg)</td>
						<td>'.$data['weight'].'</td>
					</tr>
					<tr>
						<td>Distinguished Marks</td>
						<td>'.$data['distinguished_marks'].'</td>
					</tr>
					<tr>
						<td>Nourishment</td>
						<td>'.$data['is_nourished'].'</td>
					</tr>
					<tr>
						<td>Relatives</td>
						<td>'.$rel_str.'</td>
					</tr>
					<tr>
						<td>Status of Father</td>
						<td>'.$data['fatherstat'].'</td>
					</tr>
					<tr>
						<td>Status of Mother</td>
						<td>'.$data['motherstat'].'</td>
					</tr>
					<tr>
						<td>Child Status</td>
						<td>'.$data['status_id'].'</td>
					</tr>
					<tr>
						<td>Guardian</td>
						<td>'.$gdn_str.'</td>
					</tr>
					<tr>
						<td>Child\'s Highest Educational Attainment</td>
						<td>'.$edu_str.'</td>
					</tr>
					<tr>
						<td>Child\'s Sleeping Areas</td>
						<td>'.$slp_str.'</td>
					</tr>
					<tr>
						<td>Child\'s Working Areas</td>
						<td>'.$wrk_str.'</td>
					</tr>
					<tr>
						<td>Child\'s Street Activities</td>
						<td>'.$act_str.'</td>
					</tr>
					<tr>
						<td>Documents Present</td>
						<td>'.$doc_str.'</td>
					</tr>
					<tr>
						<td>Sacraments Received</td>
						<td>'.$sac_str.'</td>
					</tr>
					<tr>
						<td>Filed by</td>
						<td>'.ucwords($vol['account_name']).'</td>
					</tr>
	    	</tbody>
	      </table>
			</div>
			<div class="col-md-1 col-lg-2"></div>
		</div>
        ';

	if($age != $data['age']){
		$q = "UPDATE profile SET age=".$age." WHERE profile_id=".$data['profile_id'];
		mysqli_query($con, $q);
	}

	}catch(Exception $e){
	}catch(Error $e){
	}finally{
		mysqli_close($con);
		echo $html;
  }
}

if(isset($_POST['active']) && !empty($_POST['active'])){
	unset($_POST['active']);

	try{
		$con = getDatabaseConnection();
		$q = "SELECT setting_id FROM settings ORDER BY setting_id DESC LIMIT 1";
		$r = mysqli_query($con, $q);
		$row = mysqli_fetch_array($r);
		if(isset($_POST['setting'])){
			$q = "UPDATE settings SET show_active=".cleanData($con, $_POST['setting']);
			mysqli_query($con,$q);
		}
	}catch(Exception $e){
	}catch(Error $e){
	}finally{
		mysqli_close($con);
	}
}

if(isset($_POST['setcheck']) && !empty($_POST['setcheck'])){
	unset($_POST['setcheck']);

	try{
		$con = getDatabaseConnection();
		$q = "SELECT show_active FROM settings ORDER BY setting_id DESC LIMIT 1";
		$r = mysqli_query($con, $q);
		$row = mysqli_fetch_array($r);
		if($row['show_active'] == 1){
			$res = '1';
			// echo json_encode(true);
		}else{
			$res = '0';
			// echo json_encode(false);
		}
	}catch(Exception $e){
	}catch(Error $e){
	}finally{
		mysqli_close($con);
		echo $res;
	}
}

if(isset($_POST['allowview']) && !empty($_POST['allowview'])){
	unset($_POST['allowview']);
	session_start();
	try{
		$con = getDatabaseConnection();
		$prv = getPrivilegeByAccountName($_SESSION['login-user']);
		if($prv == 1){
			$res = '1';
			// echo json_encode(true);
		}else{
			$res = '0';
			// echo json_encode(false);
		}
	}catch(Exception $e){
	}catch(Error $e){
	}finally{
		mysqli_close($con);
		echo $res;
	}
}

?>
