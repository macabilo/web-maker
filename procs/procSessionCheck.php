<?php
if(session_status() == PHP_SESSION_NONE){
  session_start();
}else if(session_status() == PHP_SESSION_ACTIVE && isset($_GET['logout'])){
  session_unset();
  session_destroy();
  unset($_GET['logout']);
}

//prevent unauthorized access
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');
if(!(htmlentities($_SERVER['PHP_SELF']) == "/index.php" || htmlentities($_SERVER['PHP_SELF']) == "/pages/pageLogin.php" || htmlentities($_SERVER['PHP_SELF']) == "/pages/pageRegister.php" || htmlentities($_SERVER['PHP_SELF']) == "/procs/procLogin.php" || htmlentities($_SERVER['PHP_SELF']) == "/procs/procRegister.php" || htmlentities($_SERVER['PHP_SELF']) == "/pages/pageReset.php") && !isset($_SESSION['login-user'])){
  header("Location: /index.php");
}

//prevent access without privilege
if(!(htmlentities($_SERVER['PHP_SELF']) == "/index.php") && isset($_SESSION['login-user'])){
  if (!((htmlentities($_SERVER['PHP_SELF']) == "/pages/pageProfile.php") || (htmlentities($_SERVER['PHP_SELF']) == "/pages/pageProfileList.php")) && (getPrivilegeByAccountName($_SESSION['login-user']) > 1)){
    header("Location: /index.php");
  }
}

//prevent relogging in when logged in
if((htmlentities($_SERVER['PHP_SELF']) == "/pages/pageLogin.php" || htmlentities($_SERVER['PHP_SELF']) == "/pages/pageRegister.php") && isset($_SESSION['login-user'])){
  header("Location: /index.php");
}
?>
