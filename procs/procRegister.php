<?php
include($_SERVER['DOCUMENT_ROOT'].'/procs/procSessionCheck.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');
include($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');

if(isset($_GET['register'])){
	unset($_GET['register']);
  try{
    $con = getDatabaseConnection();
    $rname = mysqli_real_escape_string($con,$_POST['realname']);
    $uname = mysqli_real_escape_string($con,$_POST['username']);
    $pword = mysqli_real_escape_string($con,$_POST['password']);
    $email = mysqli_real_escape_string($con,$_POST['email']);
    $conum = mysqli_real_escape_string($con,$_POST['contactnumber']);

    if(accountExists($uname)){
      $_SESSION['query_error'] = "Account already exists with that username!";
    }else{
      $query = "INSERT INTO account (account_id, account_name, username, password, email, phone, privilege_id, photo) VALUES (NULL, '".$rname."', '".$uname."','".md5($pword)."','".$email."','".$conum."', 3, '/photos/pp/default.jpg')";
      if(mysqli_query($con, $query)){
        $_SESSION['query_success'] = "New account added!";
      }else{
        $_SESSION['query_error'] = mysqli_error($con);
      }
    }
  }
  catch(Exception $e){
    $_SESSION['query_error'] = "Exception was caught! (".$e.")";
  }
  catch(Error $e){
    $_SESSION['query_error'] = "Error was caught! (".$e.")";
  }
  finally{
    mysqli_close($con);
    print_r($_SESSION['query_success']);
    print_r($_SESSION['query_error']);

    header('Location: /../pages/pageLogin.php');
  }
}

if(isset($_POST['approved']) && !empty($_POST['approved'])){
	unset($_POST['approved']);

  try{
    $con = getDatabaseConnection();
    $query = "UPDATE account SET privilege_id=3 WHERE account_id=".cleanData($con, $_POST['queryid']);
    if(mysqli_query($con, $query)){
      $_SESSION['query_success'] = "Pending account has been approved!";
    }else{
      $_SESSION['query_error'] = "Something went wrong";
    }
  }catch(Exception $e){
  }catch(Error $e){
  }finally{
    mysqli_close($con);
  }
}

 ?>
