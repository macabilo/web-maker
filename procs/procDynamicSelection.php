<?php
include($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');

#province is selected, retrieve localities data
if(isset($_POST['prvSelected']) && !empty($_POST['prvSelected'])){
	$prv = $_POST['prvSelected'];
	unset($_POST['prvSelected']);

	try{
		$con = getDatabaseConnection();
		$query = "SELECT * FROM locality WHERE province_id=".$prv." ORDER BY locality_name ASC";
		$result = mysqli_query($con, $query);
		$nrows = mysqli_num_rows($result);
		$arr = array();

		while($nrows > 0){
			$row = mysqli_fetch_array($result);
			$arr[] = array("id"=>$row['locality_id'], "name"=> utf8_encode($row['locality_name']));
			$nrows -= 1;
		}

	}catch(Exception $e){
		$arr[] = array("id"=>1, "name"=>$e);
	}catch(Error $e){
		$arr[] = array("id"=>2, "name"=>$e);
	}finally{
		mysqli_close($con);
		echo json_encode($arr);
	}
}

#locality selected, retrieve barangay data
if(isset($_POST['locSelected']) && !empty($_POST['locSelected'])){
	$loc = $_POST['locSelected'];
	unset($_POST['locSelected']);

	try{
		$con = getDatabaseConnection();
		$query = "SELECT * FROM barangay WHERE locality_id=".$loc." ORDER BY barangay_name ASC";
		$result = mysqli_query($con, $query);
		$nrows = mysqli_num_rows($result);
		$arr = array();

		while($nrows > 0){
			$row = mysqli_fetch_array($result);
			$arr[] = array("id"=>$row['barangay_id'], "name"=> utf8_encode($row['barangay_name']));
			$nrows -= 1;
		}

	}catch(Exception $e){
		$arr = array();
	}catch(Error $e){
		$arr = array();
	}finally{
		mysqli_close($con);
		echo json_encode($arr);
	}

}

#account selected, retrieve privilege options
if(isset($_POST['accSelected']) && !empty($_POST['accSelected'])){
	$acc = $_POST['accSelected'];
	try{
		$con = getDatabaseConnection();
		$query = "SELECT * FROM account WHERE account_id = ".$acc;
		$result = mysqli_query($con, $query);
		$data = mysqli_fetch_array($result);

		$query = "SELECT * FROM privilege WHERE privilege_id != ".$data['privilege_id'];
		$result = mysqli_query($con, $query);
		$nrows = mysqli_num_rows($result);
		$arr = array();

		while($nrows > 0){
			$row = mysqli_fetch_array($result);
			$arr[] = array("id"=>$row['privilege_id'], "name"=>$row['privilege_name']);
			$nrows -= 1;
		}
	}catch(Exception $e){
		$arr = array();
	}catch(Error $e){
		$arr = array();
	}finally{
		unset($_POST['accSelected']);
		mysqli_close($con);
		echo json_encode($arr);
	}
}

if(isset($_POST['editprof']) && !empty($_POST['editprof'])){
	$profile = $_POST['editprof'];
	try{
		$con = getDatabaseConnection();
		$q = "SELECT * FROM profile WHERE profile_id=".$profile;
		$r = mysqli_query($con,$q);
		$d = mysqli_fetch_array($r);

		$q = "SELECT DATE_FORMAT('".$d['birthdate']."', '%d %M %Y') AS bd FROM profile WHERE profile_id=".$d['profile_id'];
		$r = mysqli_query($con, $q);
		$bd = mysqli_fetch_array($r, MYSQLI_ASSOC);
		$bd = $bd['bd'];

		$q = "SELECT * FROM address WHERE address_id =". $d['address_id'];
		$r = mysqli_query($con, $q);
		$addr = mysqli_fetch_array($r);
		$q = "SELECT * FROM barangay WHERE barangay_id =".$addr['barangay_id'];
		$r = mysqli_query($con, $q);
		$bgy = mysqli_fetch_array($r);
		$q = "SELECT * FROM locality WHERE locality_id =".$bgy['locality_id'];
		$r = mysqli_query($con, $q);
		$loc = mysqli_fetch_array($r);
		$q = "SELECT * FROM province WHERE province_id =".$loc['province_id'];
		$r = mysqli_query($con, $q);
		$pro = mysqli_fetch_array($r);
		$addr_str = ucwords($addr['street_name']).", ".ucwords($bgy['barangay_name']).", ".ucwords($loc['locality_name']).", ".ucwords($pro['province_name']);

		$q = "SELECT * FROM address WHERE address_id =". $d['birthplace'];
		$r = mysqli_query($con, $q);
		$addr = mysqli_fetch_array($r);
		$q = "SELECT * FROM barangay WHERE barangay_id =".$addr['barangay_id'];
		$r = mysqli_query($con, $q);
		$bgy = mysqli_fetch_array($r);
		$q = "SELECT * FROM locality WHERE locality_id =".$bgy['locality_id'];
		$r = mysqli_query($con, $q);
		$loc = mysqli_fetch_array($r);
		$q = "SELECT * FROM province WHERE province_id =".$loc['province_id'];
		$r = mysqli_query($con, $q);
		$pro = mysqli_fetch_array($r);
		$baddr_str = ucwords($addr['street_name']).", ".ucwords($bgy['barangay_name']).", ".ucwords($loc['locality_name']).", ".ucwords($pro['province_name']);

		$q = "SELECT * FROM education WHERE education_id=".$d['education_id'];
		$r = mysqli_query($con, $q);
		$tmp = mysqli_fetch_array($r);
		$q = "SELECT level_name FROM educational_level WHERE level_id=".$tmp['level_id'];
		$r = mysqli_query($con, $q);
		$edu = mysqli_fetch_array($r);
		if($tmp['still_studying'] == 'Yes'){
			$std_str = "Yes (currently studying)";
		}else{
			$std_str = "No (out of school)";
		}

		$q = "SELECT * FROM relative WHERE relative_id=".$d['guardian_id'];
		$r = mysqli_query($con, $q);
		$gdn = mysqli_fetch_array($r);
		$q = "SELECT relationship_name FROM relationship WHERE relationship_id=".$gdn['relationship_id'];
		$r = mysqli_query($con, $q);
		$rl = mysqli_fetch_array($r);

		$q = "SELECT * FROM address WHERE address_id =". $gdn['address_id'];
		$r = mysqli_query($con, $q);
		$addr = mysqli_fetch_array($r);
		$q = "SELECT * FROM barangay WHERE barangay_id =".$addr['barangay_id'];
		$r = mysqli_query($con, $q);
		$bgy = mysqli_fetch_array($r);
		$q = "SELECT * FROM locality WHERE locality_id =".$bgy['locality_id'];
		$r = mysqli_query($con, $q);
		$loc = mysqli_fetch_array($r);
		$q = "SELECT * FROM province WHERE province_id =".$loc['province_id'];
		$r = mysqli_query($con, $q);
		$pro = mysqli_fetch_array($r);
		$gaddr_str = ucwords($addr['street_name']).", ".ucwords($bgy['barangay_name']).", ".ucwords($loc['locality_name']).", ".ucwords($pro['province_name']);

		$q = "SELECT * FROM profile_sleep_areas WHERE profile_id=".$d['profile_id'];
		$r = mysqli_query($con, $q);
		$n = mysqli_num_rows($r);
		$sa_str = "";

		while($n > 0){
			$sa = mysqli_fetch_array($r);
			$q = "SELECT area_name FROM sleeping_area WHERE area_id=".$sa['area_id'];
			$rn = mysqli_query($con, $q);
			$sn = mysqli_fetch_array($rn);
			$sa_str .= $sn['area_name'];
			if($n != 1){
				$sa_str .= ", ";
			}
			$n--;
		}

		$q = "SELECT * FROM profile_work_areas WHERE profile_id=".$d['profile_id'];
		$r = mysqli_query($con, $q);
		$n = mysqli_num_rows($r);
		$wa_str = "";

		while($n > 0){
			$wa = mysqli_fetch_array($r);
			$q = "SELECT area_name FROM working_area WHERE area_id=".$wa['area_id'];
			$rn = mysqli_query($con, $q);
			$wn = mysqli_fetch_array($rn);
			$wa_str .= $wn['area_name'];
			if($n != 1){
				$wa_str .= ", ";
			}
			$n--;
		}

		$q = "SELECT * FROM profile_activities WHERE profile_id=".$d['profile_id'];
		$r = mysqli_query($con, $q);
		$n = mysqli_num_rows($r);
		$act_str = "";

		while($n > 0){
			$wa = mysqli_fetch_array($r);
			$q = "SELECT activity_name FROM activity WHERE activity_id=".$wa['activity_id'];
			$rn = mysqli_query($con, $q);
			$wn = mysqli_fetch_array($rn);
			$act_str .= $wn['activity_name'];
			if($n != 1){
				$act_str .= ", ";
			}
			$n--;
		}

		$q = "SELECT * FROM profile_documents WHERE profile_id=".$d['profile_id'];
		$r = mysqli_query($con, $q);
		$n = mysqli_num_rows($r);
		$doc_str = "";

		while($n > 0){
			$wa = mysqli_fetch_array($r);
			$q = "SELECT document_name FROM document WHERE document_id=".$wa['document_id'];
			$rn = mysqli_query($con, $q);
			$wn = mysqli_fetch_array($rn);
			$doc_str .= $wn['document_name'];
			if($n != 1){
				$doc_str .= ", ";
			}
			$n--;
		}

		$q = "SELECT * FROM profile_sacraments WHERE profile_id=".$d['profile_id'];
		$r = mysqli_query($con, $q);
		$n = mysqli_num_rows($r);
		$sac_str = "";

		while($n > 0){
			$wa = mysqli_fetch_array($r);
			$q = "SELECT sacrament_name FROM sacrament WHERE sacrament_id=".$wa['sacrament_id'];
			$rn = mysqli_query($con, $q);
			$wn = mysqli_fetch_array($rn);
			$sac_str .= $wn['sacrament_name'];
			if($n != 1){
				$sac_str .= ", ";
			}
			$n--;
		}

		$q = "SELECT * FROM profile_relatives WHERE profile_id =".$d['profile_id'];
		// print_r("q3:".$q);
		$rels = mysqli_query($con, $q);
		$n = mysqli_num_rows($rels);
		$fam_str = "";
		while($n > 0){
			$prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
			$q = "SELECT * FROM relative WHERE relative_id=".$prof['relative_id'];
			$r = mysqli_query($con, $q);
			$rel = mysqli_fetch_array($r);
			$q = "SELECT * FROM relationship WHERE relationship_id=".$rel['relationship_id'];
			$r = mysqli_query($con, $q);
			$rv = mysqli_fetch_array($r);
			$fam_str .= ucwords($rel['relative_name'])." (".$rv['relationship_name'].")";
			if($n != 1){
				$fam_str .= ", ";
			}
			$n--;
		}

		$data = array("name"=>$d['name'],
		"alias"=>$d['alias'],
		"age"=>$d['age'],
		"religion"=>$d['religion'],
		"gender"=>$d['gender'],
		"height"=>$d['height'],
		"weight"=>$d['weight'],
		"address"=>$addr_str,
		"birthdate"=>$bd,
		"birthplace"=>$baddr_str,
		"distmarks"=>$d['distinguished_marks'],
		"edulevel"=>$edu['level_name'],
		"studying"=>$std_str,
		"yearattended"=>$tmp['last_year_attended'],
		"schoolstudied"=>$tmp['last_school_attended'],
		"nourishment"=>$d['is_nourished'],
		"family"=>$fam_str,
		"fatherstat"=>$d['fatherstat'],
		"motherstat"=>$d['motherstat'],
		"childstat"=>$d['childstat'],
		"guardian"=>$gdn['relative_name'],
		"relguardian"=>$rl['relationship_name'],
		"addrguardian"=>$gaddr_str,
		"sleepareas"=>$sa_str,
		"workareas"=>$wa_str,
		"activities"=>$act_str,
		"reason"=>$d['reason'],
		"profilestat"=>$d['status_id'],
		"document"=>$doc_str,
		"sacrament"=>$sac_str
		);

	}catch(Exception $e){
		$data = array();
	}catch(Error $e){
		$data = array();
	}finally{
		unset($_POST['editprof']) ;
		mysqli_close($con);
		echo json_encode($data);
	}
}

//select picker for relative educational and relationship
if(isset($_POST['selEdu']) && !empty($_POST['selEdu'])){
	unset($_POST['selEdu']);

	$con = getDatabaseConnection();
	$q1 = "SELECT * FROM relationship ORDER BY relationship_id ASC";
	$result1 = mysqli_query($con,$q1);
	$nrows1 = mysqli_num_rows($result1);
	$html = " <select class=\"form-control selectpicker selrelationship\" name=\"relativeRel[]\" data-live-search=\"true\" data-size=\"5\"><option selected disabled>-- select relationship to child --</option>";
	while ($nrows1 > 0){
		$rel = mysqli_fetch_array($result1);
		$html .= "<option value=".$rel['relationship_id'].">". $rel['relationship_name'] ."</option>";
		$nrows1 = $nrows1 - 1;
	}
	$html .= "</select>";

	$q2 = "SELECT * FROM educational_level ORDER BY level_id ASC";
	$result2 = mysqli_query($con,$q2);
	$nrows2 = mysqli_num_rows($result2);
	$html .= " <select class=\"form-control selectpicker seleducation\" name=\"relEducation[]\" data-live-search=\"true\" data-size=\"5\"><option selected disabled>-- select highest educational attainment --</option>";
	while ($nrows2 > 0){
		$row = mysqli_fetch_array($result2);
		$html .= "<option value=".$row['level_id'].">". $row['level_name'] ."</option>";
		$nrows2 = $nrows2 - 1;
	}
	$html .= "</select>";

	$q3 = "SELECT province_id, province_name FROM province ORDER BY province_name ASC";
	$result3 = mysqli_query($con,$q3);
	$nrows3 = mysqli_num_rows($result3);
	$html .= "<select class=\"form-control selectpicker seladdress\" name=\"relativeAddr1[]\" data-live-search=\"true\" data-size=\"5\"><option selected value=\"-1\" disabled>-- select province --</option>";
	while ($nrows3 > 0){
		$row = mysqli_fetch_array($result3);
		$html .= "<option value=".$row['province_id'].">". $row['province_name'] ."</option>";
		$nrows3 = $nrows3 - 1;
	}
	$html .= "</select>";

	mysqli_close($con);
	echo $html;
}

//dynamic selection for relative address when a province is selected
if(isset($_POST['selCity']) && !empty($_POST['selCity'])){
	unset($_POST['selCity']);
	require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');

	$con = getDatabaseConnection();
	$prv = cleanData($con, $_POST['prv']);
	$q1 = "SELECT * FROM locality WHERE province_id=".$prv." ORDER BY locality_id ASC";
	$result1 = mysqli_query($con,$q1);
	$nrows1 = mysqli_num_rows($result1);
	$html = "<option selected value=0 disabled>-- select city/municipality --</option>";
	while ($nrows1 > 0){
		$rel = mysqli_fetch_array($result1);
		$html .= "<option value=".$rel['locality_id'].">". $rel['locality_name'] ."</option>";
		$nrows1 = $nrows1 - 1;
	}

	mysqli_close($con);
	echo $html;
}

//select picker for relative educational and relationship
if(isset($_POST['selEdit']) && !empty($_POST['selEdit'])){
	unset($_POST['selEdit']);

	$con = getDatabaseConnection();
	$q1 = "SELECT * FROM relationship ORDER BY relationship_id ASC";
	$result1 = mysqli_query($con,$q1);
	$nrows1 = mysqli_num_rows($result1);
	$html = "<input type=\"text\" class=\"form-control form-half\" id=\"currentRelationship\" placeholder=\"select relative to update value\" readonly><select class=\"form-half selectpicker selrelationship\" name=\"relativeRelEdit[]\" data-live-search=\"true\" data-size=\"5\"><option selected disabled>-- select relationship to child --</option>";
	while ($nrows1 > 0){
		$rel = mysqli_fetch_array($result1);
		$html .= "<option value=".$rel['relationship_id'].">". $rel['relationship_name'] ."</option>";
		$nrows1 = $nrows1 - 1;
	}
	$html .= "</select>";

	$q2 = "SELECT * FROM educational_level ORDER BY level_id ASC";
	$result2 = mysqli_query($con,$q2);
	$nrows2 = mysqli_num_rows($result2);
	$html .= " <input type=\"text\" class=\"form-control form-half\" id=\"currentEducation\" placeholder=\"select relative to update value\" readonly><select class=\"form-half selectpicker seleducation\" name=\"relativeEduEdit[]\" data-live-search=\"true\" data-size=\"5\"><option selected disabled>-- select highest educational attainment --</option>";
	while ($nrows2 > 0){
		$row = mysqli_fetch_array($result2);
		$html .= "<option value=".$row['level_id'].">". $row['level_name'] ."</option>";
		$nrows2 = $nrows2 - 1;
	}
	$html .= "</select>";

	$q3 = "SELECT province_id, province_name FROM province ORDER BY province_name ASC";
	$result3 = mysqli_query($con,$q3);
	$nrows3 = mysqli_num_rows($result3);
	$html .= " <input type=\"text\" class=\"form-control form-half\" id=\"currentAddress1\" placeholder=\"select relative to update value\" readonly><select class=\"form-half selectpicker seladdress\" name=\"relativeAddr1Edit[]\" data-live-search=\"true\" data-size=\"5\"><option selected disabled>-- select province --</option>";
	while ($nrows3 > 0){
		$row = mysqli_fetch_array($result3);
		$html .= "<option value=".$row['province_id'].">". $row['province_name'] ."</option>";
		$nrows3 = $nrows3 - 1;
	}
	$html .= "</select>";

	mysqli_close($con);
	echo $html;
}

if(isset($_POST['selectFam']) && !empty($_POST['selectFam'])){
	$profile_id = $_POST['selectFam'];
	unset($_POST['selectFam']);

	$con = getDatabaseConnection();
	$q1 = "SELECT * FROM profile_relatives WHERE profile_id=".$profile_id;
	$result1 = mysqli_query($con,$q1);
	$nrows1 = mysqli_num_rows($result1);
	$html = " <select class=\"form-control selectpicker selrelative\" name=\"relativeId[]\" data-live-search=\"true\" data-size=\"5\"><option selected disabled>-- select relative to edit --</option>";

	while ($nrows1 > 0){
		$rel = mysqli_fetch_array($result1);
		$q = "SELECT * FROM relative WHERE relative_id=".$rel['relative_id'];
		$r = mysqli_query($con, $q);
		$n = mysqli_fetch_array($r);
		$q = "SELECT * FROM relationship WHERE relationship_id=".$n['relationship_id'];
		$r = mysqli_query($con, $q);
		$rl = mysqli_fetch_array($r);
		$html .= "<option value=".$rel['relative_id'].">". ucwords($n['relative_name']) ." (".ucwords($rl['relationship_name']).")</option>";
		$nrows1 = $nrows1 - 1;
	}
	$html .= "</select>";
	mysqli_close($con);
	echo $html;
}

if(isset($_POST['retrieveFamily']) && !empty($_POST['retrieveFamily'])){
	$id = $_POST['retrieveFamily'];
	unset($_POST['retrieveFamily']);
	require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');

	$con = getDatabaseConnection();
	$q = "SELECT * FROM relative WHERE relative_id=".$id;
	$r = mysqli_query($con, $q);
	$rel = mysqli_fetch_array($r);

	$q = "SELECT * FROM relationship WHERE relationship_id=".$rel['relationship_id'];
	$r = mysqli_query($con, $q);
	$rl = mysqli_fetch_array($r);

	$q = "SELECT * FROM educational_level WHERE level_id=".cleanData($con,$rel['education_id']);
	$r = mysqli_query($con, $q);
	if(mysqli_num_rows($r) > 0)
		$edu = mysqli_fetch_array($r)['level_name'];
	else {
		$edu = "NULL";
	}

	$q = "SELECT locality_id FROM address WHERE address_id=".cleanData($con,$rel['baddr_id']);
	$r = mysqli_query($con, $q);
	if(mysqli_num_rows($r) > 0)
		$loc = mysqli_fetch_array($r)['locality_id'];
	else {
		$loc = "NULL";
	}

	$q = "SELECT locality_name, province_id FROM locality WHERE locality_id=".$loc;
	$r = mysqli_query($con, $q);
	if(mysqli_num_rows($r) > 0){
		$ar = mysqli_fetch_array($r);
		$loc = $ar['locality_name'];
		$pro = $ar['province_id'];
	}
	else {
		$loc = "NULL";
	}

	$q = "SELECT province_name FROM province WHERE province_id=".$pro;
	$r = mysqli_query($con, $q);
	if(mysqli_num_rows($r) > 0){
		$pro = mysqli_fetch_array($r)['province_name'];
	}
	else {
		$pro = "NULL";
	}

	$data = array(
		"name"=>cleanData($con,$rel['relative_name']),
		"relationship"=>cleanData($con, $rl['relationship_name']),
		"age"=>cleanData($con, $rel['age']),
		"occupation"=>cleanData($con, $rel['occupation']),
		"salary"=>cleanData($con, $rel['salary']),
		"remarks"=>cleanData($con, $rel['remarks']),
		"contact"=>cleanData($con, $rel['contact']),
		"education"=>$edu,
		"addr1"=>$pro,
		"addr2"=>$loc,
		"maritalStat"=>cleanData($con, $rel['marital_status'])
	);

	mysqli_close($con);
	echo json_encode($data);
}
?>
