<div class="navbar-header">
  <!-- collapse button -->
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarMain">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  <a class="navbar-brand" href="#"><?php include($_SERVER['DOCUMENT_ROOT'].'/websiteName.php'); ?></a>
</div>

<div class="collapse navbar-collapse" id="navbarMain">
  <ul class="nav navbar-nav">
    <?php include($_SERVER['DOCUMENT_ROOT'].'/navs/navMainMenu.php'); ?>
  </ul>

  <ul class="nav navbar-nav navbar-right">
    <?php if(isset($_SESSION['login-user'])){
      echo "<li><a href=\"../pages/pageProfile.php\"><span class=\"glyphicon glyphicon-user\"></span> Welcome, ".$_SESSION['login-user']."!</a></li>";
    }else {
      echo "<li><a><span class=\"glyphicon glyphicon-user\"></span>Welcome!</a></li>";
    }

    if(htmlentities($_SERVER['PHP_SELF'])=='/pages/pageRegister.php'){
      echo "<li><a href=\"../pages/pageLogin.php\">";
    }else {
      echo "<li class=\"active\"><a href=\"#\">";
    }
    ?>
    <span class="glyphicon glyphicon-log-in"></span> Login</a></li>
  </ul>
</div>
